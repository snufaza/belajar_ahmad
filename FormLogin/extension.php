<?php
// FormLogin Extension for Bolt, by ahmad apandi

namespace FormLogin;

class Extension extends \Bolt\BaseExtension
{

    /**
     * Info block for FormLogin Extension.
     */
    function info()
    {

        $data = array(
            'name' => "FormLogin",
            'description' => "use {{ formlogin() }}",
            'keywords' => "login",
            'author' => "ahmad apandi",
            'link' => "akucatat@blogspot.com",
            'version' => "0.1",
            'required_bolt_version' => "1.0.2",
            'highest_bolt_version' => "1.0.2",
            'type' => "General",
            'first_releasedate' => "2013-11-14",
            'latest_releasedate' => "2013-11-14",
            'dependencies' => "",
            'priority' => 10
        );

        return $data;

    }

    /**
     * Initialize FormLogin. Called during bootstrap phase.
     */
    function init($app)
    {

        // If yourextension has a 'config.yml', it is automatically loaded.
        // $foo = $this->config['bar'];

        // Initialize the Twig function
        $this->addTwigFunction('{{ formlogin }}', 'twig{{ formlogin }}');

    }

    function initialize() {

        $this->addTwigFunction('formlogin', 'twigFormLogin');

    }

    function twigFormLogin() {
      $this->app['twig.loader.filesystem']->addPath(__DIR__);

        // if $name isn't set, use the one from the config.yml. Unless that's empty too, then use "world".
        
        // $html = $this->config['template']."
        // <div>
        //     <h5> Login {{ record.user.displayname }}  </h5>
        //     <form name='login'>
        //         <input type='text' name='username' size='10'>
        //         <input type='password' name='password' size='10'>    
        //         <input type='submit' name='submit' value='login'>
        //     </form>
        // </div>
        // ";
        $body = $this->app['twig']->render($this->config['template']);
        
        return new \Twig_Markup($body, 'UTF-8');

    }



}


