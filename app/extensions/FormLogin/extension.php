<?php
// FormLogin Extension for Bolt, by ahmad apandi

namespace FormLogin;

class Extension extends \Bolt\BaseExtension
{

    /**
     * Info block for FormLogin Extension.
     */
    function info()
    {

        $data = array(
            'name' => "FormLogin",
            'description' => "use {{ formlogin() }}",
            'keywords' => "login",
            'author' => "ahmad apandi",
            'link' => "akucatat@blogspot.com",
            'version' => "0.1",
            'required_bolt_version' => "1.0.2",
            'highest_bolt_version' => "1.0.2",
            'type' => "General",
            'first_releasedate' => "2013-11-14",
            'latest_releasedate' => "2013-11-14",
            'dependencies' => "",
            'priority' => 10
        );

        return $data;

    }

    /**
     * Initialize FormLogin. Called during bootstrap phase.
     */
    // function init($app)
    // {
    //      $this->addJquery();

    //     // // Add javascript file
    //     // $this->addJavascript("assets/asaasas.js");

    //     // // Add CSS file
    //     $this->addCSS("assets/template.css");

    //     // If yourextension has a 'config.yml', it is automatically loaded.
    //     // $foo = $this->config['bar'];

    //     // Initialize the Twig function
    //     // $this->addTwigFunction('{{ formlogin }}', 'twig{{ formlogin }}');
    //             // Make sure jQuery is included
    // }

    function initialize() {

        $this->addTwigFunction('formlogin', 'twigFormLogin');

        if (!empty($this->config['stylesheet'])) {
            $this->addCSS($this->config['stylesheet']);
        } else {
            $this->config['stylesheet'] = "";
        }

        // Set the button text.
        if (empty($this->config['button_text'])) {
            $this->config['button_text'] = "Send";
        }

    }

    function twigFormLogin() {
      $this->app['twig.loader.filesystem']->addPath(__DIR__);

        // if $name isn't set, use the one from the config.yml. Unless that's empty too, then use "world".
        
        // $html = $this->config['template']."
        // <div>
        //     <h5> Login {{ record.user.displayname }}  </h5>
        //     <form name='login'>
        //         <input type='text' name='username' size='10'>
        //         <input type='password' name='password' size='10'>    
        //         <input type='submit' name='submit' value='login'>
        //     </form>
        // </div>
        // ";

        // $data = array('username' =>'testbolt' ,
        //                 'password'=>'testbolt' );
       // $this->app['db']->insert('test', $data);  
        // $aku="asin";
        // $aku.= $app['users']->getCurrentUser();
        // $aku .= "asas";  
        // echo $aku;
        $body = $this->app['twig']->render($this->config['template']);
        return new \Twig_Markup($body, 'UTF-8');

    }



}


